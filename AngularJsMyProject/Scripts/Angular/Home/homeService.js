﻿(function () {
    'use strict';

    function homeService($http) {
        var factory = {};

        factory.homeResults = [];

        factory.gethomeResults = function () {
            return $http.get("/Home/GetUsers");
        }

        return factory;
    };

    homeService.$inject = ['$http'];

    angular
      .module('home.service', [])
      .factory('homeService', homeService);

})();