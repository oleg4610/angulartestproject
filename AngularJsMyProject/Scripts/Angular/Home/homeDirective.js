﻿(function () {
    'use strict';

    function inputChange($http, $location) {
        console.log("change test");
        return {
            restrict: 'AE',
            scope: true,
            link: function (element, attrs, ngModel) {
                console.log("change test" + ngModel);
                angular.element("#myInput").on("click", function () {
                    console.log("hover on button");
                });
            }
        };
    };

    inputChange.$inject = ['$http', '$location'];

    angular
        .module('home.directive', [])
        .directive('inputChange', inputChange);

})();