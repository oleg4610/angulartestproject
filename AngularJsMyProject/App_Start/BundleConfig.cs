﻿using System.Web;
using System.Web.Optimization;

namespace AngularJsMyProject
{
    public class BundleConfig
    {
        //Дополнительные сведения об объединении см. по адресу: http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // используйте средство сборки на сайте http://modernizr.com, чтобы выбрать только нужные тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-components").Include(
                        "~/Scripts/angular.js",
                        "~/Scripts/angular-animate.js",
                        "~/Scripts/angular-messages.js",
                        "~/Scripts/angular-ui-router.js",
                        "~/Scripts/ng-input.js",
                        "~/Scripts/ui-bootstrap-custom-2.2.0.min.js",
                        "~/Scripts/ui-bootstrap-custom-tpls-2.2.0.min.js",
                        "~/Scripts/angular-toastr.tpls.js",
                        "~/Scripts/ng-croppie.js",
                        "~/Scripts/ng-file-upload-all.min.js",
                        "~/Scripts/ng-file-upload.min.js",
                        "~/Scripts/freewall.js",
                        "~/Scripts/angulargrid.min.js"

                      ));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                      "~/Scripts/Angular/app.js",
                      "~/Scripts/Angular/Home/home.js",
                      "~/Scripts/Angular/Home/homeComponent.js",
                      "~/Scripts/Angular/Home/homeDirective.js",
                      "~/Scripts/Angular/Home/homeService.js"
                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
        }
    }
}
