﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AngularJsMyProject.Startup))]
namespace AngularJsMyProject
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
